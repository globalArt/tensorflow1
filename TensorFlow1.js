var script = document.createElement('script');
script.src = 'https://cdn.jsdelivr.net/npm/@tensorflow/tfjs@0.13.0';
document.getElementsByTagName('head')[0].appendChild(script);

// Define a model for linear regression.
const model = tf.sequential();

model.add(tf.layers.dense({
    units: 1,
    inputShape: [1]
}));

// Prepare the model for training: Specify the loss and the optimizer.
model.compile({
    loss: 'meanSquaredError',
    optimizer: 'sgd'
});

// Generate some synthetic data for training.
// const xs = tf.tensor2d([1, 2, 3, 4], [4, 1]);
// const ys = tf.tensor2d([1, 3, 5, 7], [4, 1]);

const ulaz = tf.tensor2d([-1, 0, 1, 2, 3, 4], [6, 1]);

// Iz ulaznih parametara radimo novi tenzor
const izlaz = tf.tensor2d([-5, -3, -1, 1, 3, 5], [6, 1]);

var epochCounter = 0;
var error;

async function uvjezbajMrezu() {

    // Train the model using the data.
    await model.fit(ulaz, izlaz, {
        batchSize: 1,
        epochs: 100,
        callbacks: {
            onEpochEnd: (epoch, log) => {
                ispis(epoch, log.loss)
                provjeri();
            }
        }
    });
}

function ispis(epoha, greska) {

    $("#info").html(`Uvježbavanje u tijeku - epoha: ${++epochCounter}`);
    error = greska;
}

async function uvjezbaj() {

    $("#ulaz").html("Ulaz: " + ulaz)
    $("#ulaz").append("<br/>Izlaz: " + izlaz);

    $("#info").removeClass("alert-success").addClass("alert-warning");

    var iz1 = $("#izlaz1").val();
    var iz2 = $("#izlaz2").val();
    var iz3 = $("#izlaz3").val();
    var iz4 = $("#izlaz4").val();

    // await uvjezbajMrezu(iz1, iz2, iz3, iz4);
    await uvjezbajMrezu();

    $("#info").removeClass("alert-warning").addClass("alert-success");
    // $("#info").append("<br/>Uvježbavanje završeno");
}

function provjeri() {
    // Use the model to do inference on a data point the model hasn't seen before:
    // Open the browser devtools to see the output
    // model.predict(tf.tensor2d([5], [1, 1])).print();
    var ulaz = tf.tensor2d([5], [1, 1]);
    
    // DODATI DOHVAĆANJE IZLAZA IZ MODELA
    var izlaz = model.predict(ulaz).dataSync();
    
    // console.log(ulaz[0], izlaz);

    $("#rezultat").html("Ulaz: " + ulaz);
    $("#rezultat").append("<br/>Rezultat: " + izlaz);
    $("#rezultat").append("<br/>Greska: &nbsp; " + error);
    // console.log("izlaz", izlaz);

    // $("#rez1").html(izlaz[0]);
    // $("#rez2").html(izlaz[1]);
    // $("#rez3").html(izlaz[2]);
    // $("#rez4").html(izlaz[3]);
}

$(function () {

    $("#uvjezbaj").click(uvjezbaj);

    $("#racunaj").click(provjeri);

});